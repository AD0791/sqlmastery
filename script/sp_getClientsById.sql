drop procedure if exists getClientsById;

DELIMITER $$
create procedure getClientsById(
client_id int
)
begin
	if payment_amount <= 0 then
		signal sqlstate '22003'
			set message_text = "Invalid Payment Method";
	end if;
	select * from clients c
	where c.client_id = ifnull(client_id,c.client_id);
end$$
DELIMITER ;
