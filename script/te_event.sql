drop event if exists yearly_delete_stale_ar;
delimiter $$
-- alter event
create event yearly_delete_stale_ar
on schedule
-- at "2030-01-01"
every  1 year starts '2021-01-01'
ends '2021-01-01'
do begin
	delete from payments_audit
    where action_date < now() -
    interval 1 year;
end$$
delimiter ;

alter event yearly_delete_stale_ar 
disable;