drop procedure if exists get_riskFactor;

DELIMITER $$
create procedure get_riskFactor(
)
begin
	declare rfactor decimal(9,2) 
    default 0;
    declare invTot decimal(9,2);
    declare invCount int;
    
    select count(*), 
    sum(invoice_total) into
    invCount, invTot 
    from invoices;
    
    set rfactor = invTot / invCount
    * 5;
    select rfactor;
end$$
DELIMITER ;
