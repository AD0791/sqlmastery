use sql_blog;
create fulltext index idx_title_body on posts (title,body);

select * from posts where match(title, body) against('react redux');

-- against('react -redux +form' in boolean mode);
-- against('"handling a form"' in boolean mode);