drop function if exists grisk;
delimiter $$
create function grisk(
client_id int
)
returns integer
reads sql data
begin
	declare rfactor decimal(9,2) 
    default 0;
    declare invTot decimal(9,2);
    declare invCount int;
    select count(*), 
    sum(invoice_total) into
    invCount, invTot 
    from invoices i
    where i.client_id = ifnull(client_id,i.client_id);
    set rfactor = invTot / invCount
    * 5;
    return ifnull(rfactor,0);
	return 1;
end$$
delimiter ;