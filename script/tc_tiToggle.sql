-- set session 
-- set global
show variables 
like 'transaction%';

-- can be setup by changing the last
set session 
transaction isolation 
level repeatable read;