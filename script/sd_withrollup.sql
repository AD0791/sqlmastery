use sql_invoicing;

select pm.name as Payment_method,
sum(p.amount) as Total
from payments p 
join payment_methods pm
on p.payment_method = pm.payment_method_id
group by pm.name with rollup;