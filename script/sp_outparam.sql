drop procedure if exists get_unpaid;

DELIMITER $$
create procedure get_unpaid(
client_id int,
out invoices_count int,
out invoices_total decimal(9,2)
)
begin
	if client_id <= 0 then
		signal sqlstate '22003'
			set message_text = "Invalid client";
	end if;
	select count(*), sum(invoice_total)
    into invoices_count, invoices_total -- copy to the output parameters
    from invoices i
	where i.client_id = ifnull(client_id,i.client_id) ;
end$$
DELIMITER ;
